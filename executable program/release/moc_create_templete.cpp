/****************************************************************************
** Meta object code from reading C++ file 'create_templete.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../create_templete.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'create_templete.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_create_templete_t {
    QByteArrayData data[28];
    char stringdata0[660];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_create_templete_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_create_templete_t qt_meta_stringdata_create_templete = {
    {
QT_MOC_LITERAL(0, 0, 15), // "create_templete"
QT_MOC_LITERAL(1, 16, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(4, 65, 23), // "on_pushButton_5_clicked"
QT_MOC_LITERAL(5, 89, 23), // "on_pushButton_6_clicked"
QT_MOC_LITERAL(6, 113, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(7, 137, 24), // "on_pushButton_10_clicked"
QT_MOC_LITERAL(8, 162, 23), // "on_pushButton_9_clicked"
QT_MOC_LITERAL(9, 186, 23), // "on_pushButton_8_clicked"
QT_MOC_LITERAL(10, 210, 23), // "on_pushButton_7_clicked"
QT_MOC_LITERAL(11, 234, 24), // "on_pushButton_11_clicked"
QT_MOC_LITERAL(12, 259, 24), // "on_pushButton_12_clicked"
QT_MOC_LITERAL(13, 284, 24), // "on_pushButton_18_clicked"
QT_MOC_LITERAL(14, 309, 24), // "on_pushButton_17_clicked"
QT_MOC_LITERAL(15, 334, 24), // "on_pushButton_14_clicked"
QT_MOC_LITERAL(16, 359, 24), // "on_pushButton_13_clicked"
QT_MOC_LITERAL(17, 384, 24), // "on_pushButton_15_clicked"
QT_MOC_LITERAL(18, 409, 24), // "on_pushButton_16_clicked"
QT_MOC_LITERAL(19, 434, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(20, 456, 24), // "on_pushButton_19_clicked"
QT_MOC_LITERAL(21, 481, 28), // "on_commandLinkButton_clicked"
QT_MOC_LITERAL(22, 510, 24), // "on_pushButton_20_clicked"
QT_MOC_LITERAL(23, 535, 24), // "on_pushButton_21_clicked"
QT_MOC_LITERAL(24, 560, 24), // "on_pushButton_22_clicked"
QT_MOC_LITERAL(25, 585, 24), // "on_pushButton_23_clicked"
QT_MOC_LITERAL(26, 610, 24), // "on_pushButton_24_clicked"
QT_MOC_LITERAL(27, 635, 24) // "on_pushButton_25_clicked"

    },
    "create_templete\0on_pushButton_4_clicked\0"
    "\0on_pushButton_3_clicked\0"
    "on_pushButton_5_clicked\0on_pushButton_6_clicked\0"
    "on_pushButton_2_clicked\0"
    "on_pushButton_10_clicked\0"
    "on_pushButton_9_clicked\0on_pushButton_8_clicked\0"
    "on_pushButton_7_clicked\0"
    "on_pushButton_11_clicked\0"
    "on_pushButton_12_clicked\0"
    "on_pushButton_18_clicked\0"
    "on_pushButton_17_clicked\0"
    "on_pushButton_14_clicked\0"
    "on_pushButton_13_clicked\0"
    "on_pushButton_15_clicked\0"
    "on_pushButton_16_clicked\0on_pushButton_clicked\0"
    "on_pushButton_19_clicked\0"
    "on_commandLinkButton_clicked\0"
    "on_pushButton_20_clicked\0"
    "on_pushButton_21_clicked\0"
    "on_pushButton_22_clicked\0"
    "on_pushButton_23_clicked\0"
    "on_pushButton_24_clicked\0"
    "on_pushButton_25_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_create_templete[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  144,    2, 0x08 /* Private */,
       3,    0,  145,    2, 0x08 /* Private */,
       4,    0,  146,    2, 0x08 /* Private */,
       5,    0,  147,    2, 0x08 /* Private */,
       6,    0,  148,    2, 0x08 /* Private */,
       7,    0,  149,    2, 0x08 /* Private */,
       8,    0,  150,    2, 0x08 /* Private */,
       9,    0,  151,    2, 0x08 /* Private */,
      10,    0,  152,    2, 0x08 /* Private */,
      11,    0,  153,    2, 0x08 /* Private */,
      12,    0,  154,    2, 0x08 /* Private */,
      13,    0,  155,    2, 0x08 /* Private */,
      14,    0,  156,    2, 0x08 /* Private */,
      15,    0,  157,    2, 0x08 /* Private */,
      16,    0,  158,    2, 0x08 /* Private */,
      17,    0,  159,    2, 0x08 /* Private */,
      18,    0,  160,    2, 0x08 /* Private */,
      19,    0,  161,    2, 0x08 /* Private */,
      20,    0,  162,    2, 0x08 /* Private */,
      21,    0,  163,    2, 0x08 /* Private */,
      22,    0,  164,    2, 0x08 /* Private */,
      23,    0,  165,    2, 0x08 /* Private */,
      24,    0,  166,    2, 0x08 /* Private */,
      25,    0,  167,    2, 0x08 /* Private */,
      26,    0,  168,    2, 0x08 /* Private */,
      27,    0,  169,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void create_templete::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        create_templete *_t = static_cast<create_templete *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pushButton_4_clicked(); break;
        case 1: _t->on_pushButton_3_clicked(); break;
        case 2: _t->on_pushButton_5_clicked(); break;
        case 3: _t->on_pushButton_6_clicked(); break;
        case 4: _t->on_pushButton_2_clicked(); break;
        case 5: _t->on_pushButton_10_clicked(); break;
        case 6: _t->on_pushButton_9_clicked(); break;
        case 7: _t->on_pushButton_8_clicked(); break;
        case 8: _t->on_pushButton_7_clicked(); break;
        case 9: _t->on_pushButton_11_clicked(); break;
        case 10: _t->on_pushButton_12_clicked(); break;
        case 11: _t->on_pushButton_18_clicked(); break;
        case 12: _t->on_pushButton_17_clicked(); break;
        case 13: _t->on_pushButton_14_clicked(); break;
        case 14: _t->on_pushButton_13_clicked(); break;
        case 15: _t->on_pushButton_15_clicked(); break;
        case 16: _t->on_pushButton_16_clicked(); break;
        case 17: _t->on_pushButton_clicked(); break;
        case 18: _t->on_pushButton_19_clicked(); break;
        case 19: _t->on_commandLinkButton_clicked(); break;
        case 20: _t->on_pushButton_20_clicked(); break;
        case 21: _t->on_pushButton_21_clicked(); break;
        case 22: _t->on_pushButton_22_clicked(); break;
        case 23: _t->on_pushButton_23_clicked(); break;
        case 24: _t->on_pushButton_24_clicked(); break;
        case 25: _t->on_pushButton_25_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject create_templete::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_create_templete.data,
      qt_meta_data_create_templete,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *create_templete::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *create_templete::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_create_templete.stringdata0))
        return static_cast<void*>(const_cast< create_templete*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int create_templete::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
