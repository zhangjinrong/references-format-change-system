#-------------------------------------------------
#
# Project created by QtCreator 2017-02-06T09:41:19
#
#-------------------------------------------------

QT       += core gui
QT       += core gui sql
QT       += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = iob2
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        create_templete.cpp \
    mainwindow.cpp \
    store_pice.cpp \
    tmd.cpp \
    store.cpp \
    intelligent.cpp \
    first_window.cpp \
    choose_w.cpp

HEADERS  += mainwindow.h \
    create_templete.h \
    store_pice.h \
    store.h \
    tmd.h \
    intelligent.h \
    first_window.h \
    choose_w.h

FORMS    += mainwindow.ui \
    create_templete.ui \
    intelligent.ui \
    first_window.ui \
    choose_w.ui

DISTFILES +=

RESOURCES += \
    picture.qrc
