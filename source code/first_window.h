#ifndef FIRST_WINDOW_H
#define FIRST_WINDOW_H

#include <QMainWindow>
#include"create_templete.h"
#include"mainwindow.h"
#include"tmd.h"
#include <qsqldatabase.h>
#include <QSqlQuery>
#include"store.h"
#include<QKeyEvent>
namespace Ui {
class first_window;
}

class first_window : public QMainWindow
{
    Q_OBJECT

public:
    explicit first_window(QWidget *parent = 0);
    ~first_window();
    create_templete w;
    MainWindow ww;
    tmd p[300];
private slots:
    void on_action_triggered();
    void init();

    void on_action_Q_triggered();

private:
    Ui::first_window *ui;
};

#endif // FIRST_WINDOW_H
