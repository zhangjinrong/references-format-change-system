/********************************************************************************
** Form generated from reading UI file 'create_templete.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATE_TEMPLETE_H
#define UI_CREATE_TEMPLETE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_create_templete
{
public:
    QAction *action;
    QWidget *centralwidget;
    QTextBrowser *textBrowser;
    QLabel *label;
    QLabel *label_2;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_4;
    QPushButton *pushButton_3;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_2;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QPushButton *pushButton_10;
    QPushButton *pushButton_9;
    QPushButton *pushButton_8;
    QPushButton *pushButton_7;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_4;
    QPushButton *pushButton_18;
    QPushButton *pushButton_17;
    QPushButton *pushButton_14;
    QPushButton *pushButton_13;
    QPushButton *pushButton_15;
    QPushButton *pushButton_16;
    QFrame *line;
    QTextEdit *textEdit;
    QPushButton *pushButton;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_5;
    QPushButton *pushButton_20;
    QPushButton *pushButton_21;
    QPushButton *pushButton_22;
    QPushButton *pushButton_23;
    QPushButton *pushButton_24;
    QPushButton *pushButton_25;
    QPushButton *pushButton_19;
    QFrame *line_2;
    QCommandLinkButton *commandLinkButton;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QLabel *label_4;
    QFrame *line_3;
    QLabel *label_5;
    QStatusBar *statusbar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *create_templete)
    {
        if (create_templete->objectName().isEmpty())
            create_templete->setObjectName(QStringLiteral("create_templete"));
        create_templete->resize(905, 680);
        action = new QAction(create_templete);
        action->setObjectName(QStringLiteral("action"));
        centralwidget = new QWidget(create_templete);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        textBrowser = new QTextBrowser(centralwidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(20, 510, 861, 131));
        QFont font;
        font.setPointSize(15);
        textBrowser->setFont(font);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 20, 91, 31));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(210, 20, 91, 31));
        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(20, 60, 131, 391));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_4 = new QPushButton(verticalLayoutWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        verticalLayout->addWidget(pushButton_4);

        pushButton_3 = new QPushButton(verticalLayoutWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        verticalLayout->addWidget(pushButton_3);

        pushButton_5 = new QPushButton(verticalLayoutWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        verticalLayout->addWidget(pushButton_5);

        pushButton_6 = new QPushButton(verticalLayoutWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        verticalLayout->addWidget(pushButton_6);

        pushButton_2 = new QPushButton(verticalLayoutWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);

        verticalLayoutWidget_3 = new QWidget(centralwidget);
        verticalLayoutWidget_3->setObjectName(QStringLiteral("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(210, 60, 111, 391));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        pushButton_10 = new QPushButton(verticalLayoutWidget_3);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));
        pushButton_10->setFont(font);

        verticalLayout_3->addWidget(pushButton_10);

        pushButton_9 = new QPushButton(verticalLayoutWidget_3);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));
        pushButton_9->setFont(font);

        verticalLayout_3->addWidget(pushButton_9);

        pushButton_8 = new QPushButton(verticalLayoutWidget_3);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        pushButton_8->setFont(font);

        verticalLayout_3->addWidget(pushButton_8);

        pushButton_7 = new QPushButton(verticalLayoutWidget_3);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setFont(font);

        verticalLayout_3->addWidget(pushButton_7);

        pushButton_11 = new QPushButton(verticalLayoutWidget_3);
        pushButton_11->setObjectName(QStringLiteral("pushButton_11"));
        pushButton_11->setFont(font);

        verticalLayout_3->addWidget(pushButton_11);

        pushButton_12 = new QPushButton(verticalLayoutWidget_3);
        pushButton_12->setObjectName(QStringLiteral("pushButton_12"));
        pushButton_12->setFont(font);

        verticalLayout_3->addWidget(pushButton_12);

        verticalLayoutWidget_4 = new QWidget(centralwidget);
        verticalLayoutWidget_4->setObjectName(QStringLiteral("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(350, 60, 111, 391));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        pushButton_18 = new QPushButton(verticalLayoutWidget_4);
        pushButton_18->setObjectName(QStringLiteral("pushButton_18"));
        pushButton_18->setFont(font);

        verticalLayout_4->addWidget(pushButton_18);

        pushButton_17 = new QPushButton(verticalLayoutWidget_4);
        pushButton_17->setObjectName(QStringLiteral("pushButton_17"));
        pushButton_17->setFont(font);

        verticalLayout_4->addWidget(pushButton_17);

        pushButton_14 = new QPushButton(verticalLayoutWidget_4);
        pushButton_14->setObjectName(QStringLiteral("pushButton_14"));
        pushButton_14->setFont(font);

        verticalLayout_4->addWidget(pushButton_14);

        pushButton_13 = new QPushButton(verticalLayoutWidget_4);
        pushButton_13->setObjectName(QStringLiteral("pushButton_13"));
        QFont font1;
        font1.setPointSize(15);
        font1.setBold(false);
        font1.setWeight(50);
        pushButton_13->setFont(font1);

        verticalLayout_4->addWidget(pushButton_13);

        pushButton_15 = new QPushButton(verticalLayoutWidget_4);
        pushButton_15->setObjectName(QStringLiteral("pushButton_15"));
        pushButton_15->setFont(font);

        verticalLayout_4->addWidget(pushButton_15);

        pushButton_16 = new QPushButton(verticalLayoutWidget_4);
        pushButton_16->setObjectName(QStringLiteral("pushButton_16"));
        pushButton_16->setFont(font);

        verticalLayout_4->addWidget(pushButton_16);

        line = new QFrame(centralwidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(170, 10, 20, 441));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        textEdit = new QTextEdit(centralwidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(350, 20, 104, 31));
        QFont font2;
        font2.setPointSize(12);
        textEdit->setFont(font2);
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(490, 20, 91, 31));
        verticalLayoutWidget_5 = new QWidget(centralwidget);
        verticalLayoutWidget_5->setObjectName(QStringLiteral("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(490, 60, 121, 391));
        verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        pushButton_20 = new QPushButton(verticalLayoutWidget_5);
        pushButton_20->setObjectName(QStringLiteral("pushButton_20"));

        verticalLayout_5->addWidget(pushButton_20);

        pushButton_21 = new QPushButton(verticalLayoutWidget_5);
        pushButton_21->setObjectName(QStringLiteral("pushButton_21"));

        verticalLayout_5->addWidget(pushButton_21);

        pushButton_22 = new QPushButton(verticalLayoutWidget_5);
        pushButton_22->setObjectName(QStringLiteral("pushButton_22"));

        verticalLayout_5->addWidget(pushButton_22);

        pushButton_23 = new QPushButton(verticalLayoutWidget_5);
        pushButton_23->setObjectName(QStringLiteral("pushButton_23"));

        verticalLayout_5->addWidget(pushButton_23);

        pushButton_24 = new QPushButton(verticalLayoutWidget_5);
        pushButton_24->setObjectName(QStringLiteral("pushButton_24"));

        verticalLayout_5->addWidget(pushButton_24);

        pushButton_25 = new QPushButton(verticalLayoutWidget_5);
        pushButton_25->setObjectName(QStringLiteral("pushButton_25"));

        verticalLayout_5->addWidget(pushButton_25);

        pushButton_19 = new QPushButton(centralwidget);
        pushButton_19->setObjectName(QStringLiteral("pushButton_19"));
        pushButton_19->setGeometry(QRect(670, 20, 111, 31));
        line_2 = new QFrame(centralwidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(630, 20, 20, 421));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        commandLinkButton = new QCommandLinkButton(centralwidget);
        commandLinkButton->setObjectName(QStringLiteral("commandLinkButton"));
        commandLinkButton->setGeometry(QRect(660, 210, 151, 51));
        commandLinkButton->setAutoRepeatDelay(297);
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(660, 130, 121, 31));
        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(660, 180, 151, 20));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(660, 270, 191, 51));
        line_3 = new QFrame(centralwidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(20, 460, 861, 16));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 480, 131, 21));
        create_templete->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(create_templete);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        create_templete->setStatusBar(statusbar);
        toolBar = new QToolBar(create_templete);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        create_templete->addToolBar(Qt::TopToolBarArea, toolBar);

        toolBar->addSeparator();

        retranslateUi(create_templete);

        QMetaObject::connectSlotsByName(create_templete);
    } // setupUi

    void retranslateUi(QMainWindow *create_templete)
    {
        create_templete->setWindowTitle(QApplication::translate("create_templete", "MainWindow", 0));
        action->setText(QApplication::translate("create_templete", "\345\210\233\345\273\272\346\250\241\346\235\277", 0));
        label->setText(QApplication::translate("create_templete", "<html><head/><body><p><span style=\" font-size:16pt;\">\345\217\230\351\207\217\346\214\211\351\222\256</span></p></body></html>", 0));
        label_2->setText(QApplication::translate("create_templete", "<html><head/><body><p><span style=\" font-size:16pt;\">\345\270\270\351\207\217\346\214\211\351\222\256</span></p></body></html>", 0));
        pushButton_4->setText(QApplication::translate("create_templete", "\345\247\223\346\260\217", 0));
        pushButton_3->setText(QApplication::translate("create_templete", "\345\220\215\345\255\227", 0));
        pushButton_5->setText(QApplication::translate("create_templete", "\350\256\272\346\226\207\345\220\215\345\255\227", 0));
        pushButton_6->setText(QApplication::translate("create_templete", "\346\234\237\345\210\212", 0));
        pushButton_2->setText(QApplication::translate("create_templete", "\346\227\245\346\234\237", 0));
        pushButton_10->setText(QApplication::translate("create_templete", "\351\200\227\357\274\210,\357\274\211", 0));
        pushButton_9->setText(QApplication::translate("create_templete", "\347\202\271\357\274\210.\357\274\211", 0));
        pushButton_8->setText(QApplication::translate("create_templete", "\347\251\272\357\274\210 \357\274\211", 0));
        pushButton_7->setText(QApplication::translate("create_templete", "\"", 0));
        pushButton_11->setText(QApplication::translate("create_templete", "(", 0));
        pushButton_12->setText(QApplication::translate("create_templete", ")", 0));
        pushButton_18->setText(QApplication::translate("create_templete", "[", 0));
        pushButton_17->setText(QApplication::translate("create_templete", "]", 0));
        pushButton_14->setText(QApplication::translate("create_templete", "{", 0));
        pushButton_13->setText(QApplication::translate("create_templete", "}", 0));
        pushButton_15->setText(QApplication::translate("create_templete", ":", 0));
        pushButton_16->setText(QApplication::translate("create_templete", "-", 0));
        pushButton->setText(QApplication::translate("create_templete", " \346\267\273\345\212\240\345\270\270\351\207\217", 0));
        pushButton_20->setText(QString());
        pushButton_21->setText(QString());
        pushButton_22->setText(QString());
        pushButton_23->setText(QString());
        pushButton_24->setText(QString());
        pushButton_25->setText(QString());
        pushButton_19->setText(QApplication::translate("create_templete", "\346\222\244\351\224\200", 0));
        commandLinkButton->setText(QApplication::translate("create_templete", "\345\255\230\345\202\250\346\250\241\346\235\277\n"
"\344\270\213\344\270\200\346\255\245", 0));
        label_3->setText(QApplication::translate("create_templete", "<html><head/><body><p><span style=\" font-size:18pt;\">\346\250\241\346\235\277\345\220\215\345\255\227\357\274\232</span></p></body></html>", 0));
        label_4->setText(QApplication::translate("create_templete", "<html><head/><body><p><span style=\" font-size:18pt;\">\350\257\267\350\276\223\345\205\245\346\250\241\346\235\277\345\220\215\345\255\227\357\274\201\357\274\201\357\274\201</span></p></body></html>", 0));
        label_5->setText(QApplication::translate("create_templete", "<html><head/><body><p><span style=\" font-size:14pt;\">\346\230\276\347\244\272\346\241\206\357\274\232</span></p></body></html>", 0));
        toolBar->setWindowTitle(QApplication::translate("create_templete", "toolBar", 0));
    } // retranslateUi

};

namespace Ui {
    class create_templete: public Ui_create_templete {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATE_TEMPLETE_H
