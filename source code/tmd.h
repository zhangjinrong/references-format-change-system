#ifndef TMD_H
#define TMD_H

//#include <QDeclarativeItem>
#include <QMainWindow>
#include <QObject>
//#include <QQuickItem>
#include <QSharedDataPointer>
#include <QWidget>
#include"store.h"

class tmdData;

class tmd : public QObject
{
    Q_OBJECT
public:
    explicit tmd(QObject *parent = 0);
    tmd(const tmd &);
    tmd &operator=(const tmd &);
    ~tmd();
    int num;
    int init_num;
signals:
void put(int);

public slots:
void hello(bool);
void init();
private:
    QSharedDataPointer<tmdData> data;
};

#endif // TMD_H
