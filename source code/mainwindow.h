#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "store_pice.h"
#include"tmd.h"
#include <QMainWindow>
#include <QCheckBox>
#include <QPushButton>
#include"store.h"
#include"choose_w.h"
#include <qdialog.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void first_chai();
    tmd dd[100];
    int important[100];
    void loadFile(const QString &);
    void find_next();
    QString im_ss;
    QString file_ss;
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void deal();

    void deal1();

    void on_pushButton_5_clicked();

    void add_etal();

    void on_action_triggered();

    void on_pushButton_6_clicked();

    void on_action_2_triggered();

    void on_action_3_triggered();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

   // void on_pushButton_9_clicked();

private:
    Ui::MainWindow *ui;
    QCheckBox *item[100];
    QPushButton *button[100];
};

#endif // MAINWINDOW_H
