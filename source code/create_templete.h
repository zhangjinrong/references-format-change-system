#ifndef CREATE_TEMPLETE_H
#define CREATE_TEMPLETE_H
#include <QMainWindow>
#include<QPushButton>
#include<qdebug.h>
#include<QObject>
#include<qpushbutton.h>
#include <QtWidgets>
#include"mainwindow.h"
#include"store.h"
#include <qsqldatabase.h>
#include <QSqlQuery>

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QPlainTextEdit;
class QPushButton;
class QGroupBox;
class QDialogButtonBox;
QT_END_NAMESPACE

namespace Ui {
class create_templete;
}

class create_templete : public QMainWindow
{
    Q_OBJECT

public:
    explicit create_templete(QWidget *parent = 0);
    ~create_templete();
    Ui::create_templete *ui;
    QPushButton *ppp;
    MainWindow w;

public slots:

private slots:
    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_18_clicked();

    void on_pushButton_17_clicked();

    void on_pushButton_14_clicked();

    void on_pushButton_13_clicked();

    void on_pushButton_15_clicked();

    void on_pushButton_16_clicked();

    void on_pushButton_clicked();

    void on_pushButton_19_clicked();

    void on_commandLinkButton_clicked();





    void on_pushButton_20_clicked();

    void on_pushButton_21_clicked();

    void on_pushButton_22_clicked();

    void on_pushButton_23_clicked();

    void on_pushButton_24_clicked();

    void on_pushButton_25_clicked();



private:

    QPushButton *add_button[6];
  //  MainWindow w;


};

#endif // CREATE_TEMPLETE_H
