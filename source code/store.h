#ifndef STORE_H
#define STORE_H
#include<QString>
int put();
int get(int);
void get_string(QString);
void get_common(int,QString);
void get_common_num(int);
void get_important(int *);
int *put_important();
void get_init_num(int);
int put_init_num();
QString put_string();
#endif // STORE_H
