/********************************************************************************
** Form generated from reading UI file 'choose_w.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHOOSE_W_H
#define UI_CHOOSE_W_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_choose_w
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;

    void setupUi(QDialog *choose_w)
    {
        if (choose_w->objectName().isEmpty())
            choose_w->setObjectName(QStringLiteral("choose_w"));
        choose_w->resize(368, 147);
        pushButton = new QPushButton(choose_w);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(30, 40, 81, 61));
        pushButton_2 = new QPushButton(choose_w);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(140, 40, 81, 61));
        pushButton_3 = new QPushButton(choose_w);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(250, 40, 81, 61));

        retranslateUi(choose_w);

        QMetaObject::connectSlotsByName(choose_w);
    } // setupUi

    void retranslateUi(QDialog *choose_w)
    {
        choose_w->setWindowTitle(QApplication::translate("choose_w", "Dialog", 0));
        pushButton->setText(QApplication::translate("choose_w", "\350\246\206\347\233\226\345\216\237\346\234\211", 0));
        pushButton_2->setText(QApplication::translate("choose_w", "\350\277\275\345\212\240\345\206\205\345\256\271", 0));
        pushButton_3->setText(QApplication::translate("choose_w", "\351\207\215\346\226\260\351\200\211\346\213\251", 0));
    } // retranslateUi

};

namespace Ui {
    class choose_w: public Ui_choose_w {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHOOSE_W_H
