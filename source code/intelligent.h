#ifndef INTELLIGENT_H
#define INTELLIGENT_H

#include <QMainWindow>
#include <qdebug.h>
namespace Ui {
class intelligent;
}

class intelligent : public QMainWindow
{
    Q_OBJECT

public:
    explicit intelligent(QWidget *parent = 0);
    ~intelligent();
    void deal();
    int total_order[5];//姓，名，文献名字，期刊，日期
    int data_order[5];//年，数字，括号，区间1，区间2
private:
    Ui::intelligent *ui;
};

#endif // INTELLIGENT_H
