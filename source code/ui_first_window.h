/********************************************************************************
** Form generated from reading UI file 'first_window.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FIRST_WINDOW_H
#define UI_FIRST_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_first_window
{
public:
    QAction *action;
    QAction *action456;
    QAction *action_Q;
    QWidget *centralwidget;
    QTextBrowser *textBrowser;
    QTextBrowser *textBrowser_2;
    QLabel *label;
    QLabel *label_3;
    QMenuBar *menubar;
    QMenu *menu;
    QMenu *menu_2;
    QMenu *menu_Q;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *first_window)
    {
        if (first_window->objectName().isEmpty())
            first_window->setObjectName(QStringLiteral("first_window"));
        first_window->resize(976, 666);
        action = new QAction(first_window);
        action->setObjectName(QStringLiteral("action"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/new/prefix1/new.png"), QSize(), QIcon::Active, QIcon::On);
        action->setIcon(icon);
        action456 = new QAction(first_window);
        action456->setObjectName(QStringLiteral("action456"));
        action_Q = new QAction(first_window);
        action_Q->setObjectName(QStringLiteral("action_Q"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/new/prefix1/close.png"), QSize(), QIcon::Active, QIcon::On);
        action_Q->setIcon(icon1);
        action_Q->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        centralwidget = new QWidget(first_window);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        textBrowser = new QTextBrowser(centralwidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(160, 180, 721, 421));
        textBrowser_2 = new QTextBrowser(centralwidget);
        textBrowser_2->setObjectName(QStringLiteral("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(160, 30, 721, 131));
        QFont font;
        font.setFamily(QStringLiteral("Andalus"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        textBrowser_2->setFont(font);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 30, 81, 121));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(50, 260, 54, 221));
        first_window->setCentralWidget(centralwidget);
        menubar = new QMenuBar(first_window);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 976, 23));
        menu = new QMenu(menubar);
        menu->setObjectName(QStringLiteral("menu"));
        menu_2 = new QMenu(menubar);
        menu_2->setObjectName(QStringLiteral("menu_2"));
        menu_Q = new QMenu(menubar);
        menu_Q->setObjectName(QStringLiteral("menu_Q"));
        first_window->setMenuBar(menubar);
        statusbar = new QStatusBar(first_window);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        first_window->setStatusBar(statusbar);

        menubar->addAction(menu->menuAction());
        menubar->addAction(menu_2->menuAction());
        menubar->addAction(menu_Q->menuAction());
        menu_2->addAction(action);
        menu_Q->addAction(action_Q);

        retranslateUi(first_window);

        QMetaObject::connectSlotsByName(first_window);
    } // setupUi

    void retranslateUi(QMainWindow *first_window)
    {
        first_window->setWindowTitle(QApplication::translate("first_window", "MainWindow", 0));
        action->setText(QApplication::translate("first_window", "\345\210\233\345\273\272\346\250\241\346\235\277", 0));
        action456->setText(QApplication::translate("first_window", "456", 0));
        action_Q->setText(QApplication::translate("first_window", "\351\200\200\345\207\272", 0));
        textBrowser->setHtml(QApplication::translate("first_window", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\345\210\233\345\273\272\346\250\241\346\235\277\346\265\201\347\250\213\357\274\232</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\343\200\2201\343\200\221\345\215\225\345\207\273\346\234\254\347\250\213\345\272\217\347\225\214\351\235\242\344\270\212\347\232\204\342\200\234\345\210\233\345\273\272\346\250\241\346\235\277\342\200"
                        "\235\350\217\234\345\215\225\344\270\255\347\232\204\342\200\234\345\210\233\345\273\272\346\250\241\346\235\277\342\200\235\345\221\275\344\273\244\357\274\214\345\217\257\344\273\245\346\211\223\345\274\200\342\200\234\345\210\233\345\273\272\346\250\241\346\235\277\342\200\235\347\252\227\345\217\243\350\277\233\350\241\214\346\226\260\346\250\241\346\235\277\347\232\204\345\210\233\345\273\272\343\200\202\345\234\250\350\257\245\347\252\227\345\217\243\344\270\255\346\230\276\347\244\272\344\272\206\345\217\230\351\207\217\346\214\211\351\222\256\357\274\210\347\224\250\344\272\216\345\214\272\345\210\253\344\270\215\345\220\214\347\261\273\345\236\213\345\217\230\351\207\217\345\220\215\347\247\260\357\274\211\345\222\214\345\270\270\351\207\217\346\214\211\351\222\256\357\274\210\347\224\250\344\272\216\350\256\276\347\275\256\346\250\241\346\235\277\344\270\255\347\232\204\350\277\236\346\216\245\345\255\227\347\254\246\357\274\214\345\246\202\351\200\227\345\217\267\343\200\201\347\202\271\345\217\267\347"
                        "\255\211\357\274\214\345\215\225\345\207\273\342\200\234\346\267\273\345\212\240\345\270\270\351\207\217\342\200\235\346\214\211\351\222\256\345\217\257\350\277\233\350\241\214\346\267\273\345\212\240\345\270\270\351\207\217\346\214\211\351\222\256\357\274\211\343\200\202\345\215\225\345\207\273\345\217\230\351\207\217\346\214\211\351\222\256\345\222\214\345\270\270\351\207\217\346\214\211\351\222\256\346\240\271\346\215\256\351\234\200\350\246\201\350\277\233\350\241\214\346\250\241\346\235\277\347\232\204\350\256\276\350\256\241\357\274\214\345\271\266\345\234\250\342\200\234\346\230\276\347\244\272\346\226\207\346\234\254\346\241\206\342\200\235\344\270\255\350\277\233\350\241\214\346\230\276\347\244\272\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\343\200\2202\343\200\221\350\276\223\345\205\245\346\226\260\346\250\241\346\235\277"
                        "\347\232\204\345\220\215\345\255\227\357\274\214\345\215\225\345\207\273\342\200\234\345\255\230\345\202\250\346\250\241\346\235\277 \344\270\213\344\270\200\346\255\245\342\200\235\346\214\211\351\222\256\344\277\235\345\255\230\346\226\260\345\210\233\345\273\272\347\232\204\346\250\241\346\235\277\357\274\214\345\271\266\350\277\233\345\205\245\342\200\234\345\274\200\345\247\213\350\275\254\346\215\242\342\200\235\347\252\227\345\217\243\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\343\200\2203\343\200\221\345\210\233\345\273\272\345\220\216\347\232\204\346\250\241\346\235\277\344\274\232\350\207\252\345\212\250\344\277\235\345\255\230\357\274\214\344\270\213\346\254\241\344\275\277\347\224\250\346\227\266\345\217\257\345\234\250\346\234\254\347\252\227\345\217\243\344\270\255\351\200\232\350\277\207\345\215\225\345\207\273\342\200"
                        "\234\346\237\245\347\234\213\346\250\241\346\235\277\342\200\235\344\270\213\346\213\211\350\217\234\345\215\225\350\277\233\350\241\214\346\237\245\347\234\213\357\274\214\351\200\211\346\213\251\357\274\214\345\210\240\351\231\244\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\345\274\200\345\247\213\350\275\254\346\215\242\346\265\201\347\250\213\357\274\232</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\343\200\2201\343\200\221\345\260\206\345\276\205\350\275\254\346\215\242\345\206\205\345\256\271\344\277\235\345\255\230\345\234\250\346\226\207\346\234\254\346\226\207\344\273\266\344\270\255\357\274\210.txt\357\274\211\357\274\214\345\271\266\346\214\211\347\205\247[1],"
                        "[2],[3]\342\200\246\342\200\246\347\232\204\351\241\272\345\272\217\346\216\222\345\210\227\343\200\202\347\202\271\345\207\273\342\200\234\351\200\211\346\213\251\345\206\205\345\256\271\342\200\235\350\217\234\345\215\225\344\270\255\347\232\204\342\200\234\351\200\211\346\213\251\350\276\223\345\205\245\345\206\205\345\256\271\342\200\235\345\221\275\344\273\244\346\210\226\345\267\245\345\205\267\346\240\217\344\270\212\347\232\204\342\200\234\351\200\211\346\213\251\350\276\223\345\205\245\345\206\205\345\256\271\342\200\235\346\214\211\351\222\256\357\274\214\351\200\211\346\213\251\345\276\205\350\275\254\346\215\242\347\232\204\346\226\207\344\273\266\343\200\202\347\202\271\345\207\273\342\200\234\351\200\211\346\213\251\350\276\223\345\207\272\345\206\205\345\256\271\342\200\235\345\221\275\344\273\244\346\210\226\345\267\245\345\205\267\346\240\217\344\270\212\347\232\204\342\200\234\351\200\211\346\213\251\350\276\223\345\207\272\345\206\205\345\256\271\342\200\235\346\214\211\351\222\256\357\274\214"
                        "\351\200\211\346\213\251\350\276\223\345\207\272\346\226\207\344\273\266\357\274\210.txt\357\274\211\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\343\200\2202\343\200\221\347\202\271\345\207\273\342\200\234\345\274\200\345\247\213\342\200\235\346\214\211\351\222\256\357\274\214\344\274\232\345\260\206\350\276\223\345\205\245\346\241\206\344\270\255\347\232\204\345\206\205\345\256\271\344\273\245\351\200\211\351\241\271\347\232\204\346\226\271\345\274\217\345\234\250\344\270\213\346\226\271\346\230\276\347\244\272\357\274\214\345\217\257\351\200\232\350\277\207\345\257\271\350\277\231\344\272\233\351\200\211\351\241\271\347\232\204\351\200\211\346\213\251\350\277\233\350\241\214\346\213\206\345\210\206\346\210\226\345\220\210\345\271\266\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin"
                        "-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\343\200\2203\343\200\221\347\202\271\345\207\273\342\200\234\344\270\213\344\270\200\346\255\245\342\200\235\346\214\211\351\222\256\357\274\214\345\260\206\344\273\245\346\214\211\351\222\256\345\275\242\345\274\217\346\230\276\347\244\272\344\270\212\344\270\200\346\255\245\347\232\204\345\220\204\344\270\252\351\200\211\351\241\271\343\200\202\351\200\232\350\277\207\347\202\271\345\207\273\350\277\231\344\272\233\346\214\211\351\222\256\345\217\257\345\260\206\350\276\223\345\207\272\346\241\206\344\270\255\347\232\204\345\206\205\345\256\271\350\277\233\350\241\214\347\233\270\345\257\271\345\272\224\347\232\204\346\233\277\346\215\242\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\343\200\2204\343\200\221\347"
                        "\202\271\345\207\273\342\200\234\344\270\213\344\270\200\346\235\241\342\200\235\346\214\211\351\222\256\345\217\257\345\260\206\350\275\254\346\215\242\345\220\216\347\232\204\345\206\205\345\256\271\345\255\230\345\205\245\350\276\223\345\207\272\346\226\207\344\273\266\344\270\255\357\274\214\345\220\214\346\227\266\345\234\250\342\200\234\350\276\223\345\205\245\346\241\206\342\200\235\344\270\255\346\230\276\347\244\272\345\276\205\350\275\254\346\215\242\347\232\204\344\270\213\344\270\200\346\235\241\345\206\205\345\256\271\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'\345\256\213\344\275\223'; font-size:16pt;\">\343\200\2205\343\200\221\345\215\225\345\207\273\342\200\234\351\207\215\346\226\260\346\213\206\345\210\206\342\200\235\345\222\214\342\200\234\351\207\215\346\226\260\346\233\277\346\215\242\342\200\235\346\214\211\351\222\256\345\217\257\345\257\271\344\270\212"
                        "\344\270\200\346\255\245\346\223\215\344\275\234\350\277\233\350\241\214\351\207\215\346\226\260\346\213\206\345\210\206\345\222\214\351\207\215\346\226\260\346\233\277\346\215\242\343\200\202</span></p></body></html>", 0));
        label->setText(QApplication::translate("first_window", "<html><head/><body><p><span style=\" font-size:28pt;\">\346\250\241\346\235\277</span></p><p><span style=\" font-size:28pt;\">\345\261\225\347\244\272</span></p></body></html>", 0));
        label_3->setText(QApplication::translate("first_window", "<html><head/><body><p><span style=\" font-size:28pt;\">\346\223\215</span></p><p><span style=\" font-size:28pt;\">\344\275\234</span></p><p><span style=\" font-size:28pt;\">\350\257\264</span></p><p><span style=\" font-size:28pt;\">\346\230\216</span></p></body></html>", 0));
        menu->setTitle(QApplication::translate("first_window", "&\346\237\245\347\234\213\346\250\241\346\235\277(F)", 0));
        menu_2->setTitle(QApplication::translate("first_window", "&\345\210\233\345\273\272\346\250\241\346\235\277(C)", 0));
        menu_Q->setTitle(QApplication::translate("first_window", "&\351\200\200\345\207\272(Q)", 0));
    } // retranslateUi

};

namespace Ui {
    class first_window: public Ui_first_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FIRST_WINDOW_H
