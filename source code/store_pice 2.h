#ifndef STORE_PICE_H
#define STORE_PICE_H

#include <QObject>
#include "store.h"

class store_pice : public QObject
{
    Q_OBJECT
public:
    explicit store_pice(QObject *parent = 0);
    int num;
signals:

public slots:
    void s_pice();
};

#endif // STORE_PICE_H
