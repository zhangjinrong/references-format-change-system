#ifndef CHOOSE_W_H
#define CHOOSE_W_H

#include <QDialog>

namespace Ui {
class choose_w;
}

class choose_w : public QDialog
{
    Q_OBJECT

public:
    explicit choose_w(QWidget *parent = 0);
    ~choose_w();
    int key;

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::choose_w *ui;
};

#endif // CHOOSE_W_H
