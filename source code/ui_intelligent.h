/********************************************************************************
** Form generated from reading UI file 'intelligent.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INTELLIGENT_H
#define UI_INTELLIGENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_intelligent
{
public:
    QWidget *centralwidget;
    QTextBrowser *textBrowser;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *intelligent)
    {
        if (intelligent->objectName().isEmpty())
            intelligent->setObjectName(QStringLiteral("intelligent"));
        intelligent->resize(800, 600);
        centralwidget = new QWidget(intelligent);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        textBrowser = new QTextBrowser(centralwidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(60, 10, 691, 101));
        QFont font;
        font.setFamily(QStringLiteral("Aharoni"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        textBrowser->setFont(font);
        intelligent->setCentralWidget(centralwidget);
        menubar = new QMenuBar(intelligent);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 23));
        intelligent->setMenuBar(menubar);
        statusbar = new QStatusBar(intelligent);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        intelligent->setStatusBar(statusbar);

        retranslateUi(intelligent);

        QMetaObject::connectSlotsByName(intelligent);
    } // setupUi

    void retranslateUi(QMainWindow *intelligent)
    {
        intelligent->setWindowTitle(QApplication::translate("intelligent", "MainWindow", 0));
        textBrowser->setHtml(QApplication::translate("intelligent", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Aharoni'; font-size:20pt; font-weight:600; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'SimSun'; font-size:9pt; font-weight:400;\">Wu XD, Xie F, Huang YM, Hu XG, Gao J. Mining sequential patterns with wildcards and the One-Off condition.Ruan Jian Xue Bao/Journal of Software, 2013,24(8):1804\342\210\2221815.</span></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class intelligent: public Ui_intelligent {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INTELLIGENT_H
